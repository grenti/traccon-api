package models

import "fmt"

// Name ... group of names for an individual
type Name struct {
	First string `json:"first"`
	Last  string `json:"last"`
}

func (n Name) PrintLastFirst() string {
	return fmt.Sprintf("%s, %s", n.Last, n.First)
}

func (n Name) PrintFirstLast() string {
	return fmt.Sprintf("%s, %s", n.First, n.Last)
}
