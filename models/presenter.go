package models

import (
	"labix.org/v2/mgo/bson"
)

// Presenter ... individual presenting topic/talk at conference
type Presenter struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	Name      Name          `json:"name"`
	Position  string        `json:"position"`
	Employer  string        `json:"employer"`
	Biography string        `json:"biography"`
	Contacts  []Contact     `json:"contacts"`
	Reviews   []Review      `json:"reviews"`
}
