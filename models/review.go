package models

// Rating ... participants score on quality of session
type Rating int

const (
	Terrible     Rating = 1
	BelowAverage Rating = 2
	Average      Rating = 3
	Good         Rating = 4
	Excellent    Rating = 5
)

func (r Rating) String() string {
	switch r {
	case Terrible:
		return "Terrible"
	case BelowAverage:
		return "Below Average"
	case Average:
		return "Average"
	case Good:
		return "Good"
	case Excellent:
		return "Excellent"
	default:
		return "None"
	}
}

// Review ... session rating and review information
type Review struct {
	Rating      Rating `json:"rating"`
	Description string `json:"description"`
}
