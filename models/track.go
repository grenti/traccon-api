package models

import (
	"labix.org/v2/mgo/bson"
)

// Track ... grouping of sessions with similar topics
type Track struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string
	Description string
}
