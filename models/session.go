package models

import (
	"labix.org/v2/mgo/bson"
)

// Session ... presentation from a speaker at a conference
type Session struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Description string        `json:"description" bson:"description"`
	Duration    int           `json:"duration" bson:"duration"`
	Level       int           `json:"level" bson:"level"`
	Presenter   Presenter     `json:"presenter" bson:"presenter"`
	Track       Track         `json:"track"  bson:"track"`
	Reviews     []Review      `json:"reviews" bson:"reviews"`
}

// Level ... comprehension level of session
type Level int

const (
	Beginner     Level = 1
	Intermediate Level = 2
	Advanced     Level = 3
)

func (l Level) String() string {
	switch l {
	case Beginner:
		return "Beginner"
	case Intermediate:
		return "Intermediate"
	case Advanced:
		return "Advanced"
	default:
		return "None"
	}
}
