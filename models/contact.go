package models

// ContactType ... Type of contact info
type ContactType int

const (
	Email      ContactType = 1
	Phone      ContactType = 2
	Web        ContactType = 3
	Twitter    ContactType = 4
	Facebook   ContactType = 5
	GooglePlus ContactType = 6
)

func (c ContactType) String() string {
	switch c {
	case Email:
		return "Email"
	case Phone:
		return "Phone"
	case Web:
		return "Web"
	case Twitter:
		return "Twitter"
	case Facebook:
		return "Facebook"
	case GooglePlus:
		return "Google+"
	default:
		return "None"
	}
}

// Contact ... means to contact presenter
type Contact struct {
	Type  ContactType `json:"type"`
	Value string      `json:"value"`
}
