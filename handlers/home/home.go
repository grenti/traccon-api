package home

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Index ... handler to handle / route
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// if r.URL.Path != "/" {
	// 	return
	// }
	fmt.Fprintf(w, "Welcome to Traccon API!")
	log.Print("Handled the / route")
}
