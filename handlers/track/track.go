package track

import (
	"log"
	"net/http"

	"bitbucket.org/grenti/traccon-api/models"

	"encoding/json"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// GetTracks ... Handler method for HTTP GET /tracks. Retrieves all
// tracks in the system. No paging available yet
func GetTracks(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	c := session.DB("traccon_dev").C("tracks")

	tracks := []models.Track{}

	err = c.Find(nil).All(&tracks)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Error processing response", 500)
		return
	}

	json.NewEncoder(w).Encode(tracks)
}

// GetTrack ... Handler method for HTTP GET /tracks/:id. Retrieves track
// for supplied {id} if it exists.
func GetTrack(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	log.Printf("Handling path /track/%s", p.ByName("id"))
	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	c := session.DB("traccon_dev").C("tracks")

	track := models.Track{}
	err = c.FindId(bson.ObjectIdHex(p.ByName("id"))).One(&track)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Error Processing Request", 500)
		return
	}

	json.NewEncoder(w).Encode(track)
}
