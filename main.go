package main

import (
	"net/http"

	"bitbucket.org/grenti/traccon-api/router"
)

func main() {
	httpRouter := router.Register()
	http.ListenAndServe(":8479", httpRouter)
}
