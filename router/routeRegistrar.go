package router

import (
	"bitbucket.org/grenti/traccon-api/handlers/home"
	"bitbucket.org/grenti/traccon-api/handlers/track"
	"github.com/julienschmidt/httprouter"
)

// Register ... method that registers all configurable routes and handlers
func Register() *httprouter.Router {
	router := httprouter.New()
	router.GET("/", home.Index)
	router.GET("/tracks", track.GetTracks)
	router.GET("/tracks/:id", track.GetTrack)

	return router
}
